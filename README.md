# rabox

## Video streaming on Raspberry PI

```
sudo nano /etc/systemd/system/rabox.service
```

```
[Unit]
Description=RABOX running on Raspberry Pi
Requires=rc-local.service

[Service]
WorkingDirectory=/opt/rabox
ExecStart=/opt/rabox/run.sh
Restart=always
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=rabox
User=root

[Install]
WantedBy=multi-user.target
```

# enable service

```
sudo systemctl enable rabox.service
```

# setup remote.it

```
sudo apt install connectd
sudo connectd_startup_control
```

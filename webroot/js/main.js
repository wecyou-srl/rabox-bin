window.HELP_IMPROVE_VIDEOJS = false;

var videos = [];
var hostname = new URL(window.location).hostname;
var proxy = getParam('proxy');
var rtmp = getParam('rtmp');
var table;

if (window.location.port == 8080) {
    hostname = 'ZenNIO';
}

function getParam(name){
    const parts = window.location.href.split('?');
    if (parts.length > 1) {
        name = encodeURIComponent(name);
        const params = parts[1].split('&');
        const found = params.filter(el => (el.split('=')[0] === name) && el);
        if (found.length) {
            let value = decodeURIComponent(found[0].split('=')[1]);
            localStorage.setItem(name, value);
            return value;
        }
    }
    
    if (localStorage.getItem(name)) {
        return localStorage.getItem(name);
    }
    return '';
}

function unauthorized() {
    alert('401 - Unauthorized');
    location.href = '/auth';
}

function live() {
    fetch('/api/live', {
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Authorization': localStorage.getItem('token')
        },
        method : 'GET',
        cache: 'no-cache'
    })
    .then(response => response.status == 401 ? unauthorized() : response.json())
    .then(json => appendLive(json))
    .catch(error => console.log(error));
}

function appendLive(json) {
    if (json.length === 0) {
        document.getElementById('container').innerHTML = '<h3>No streams found</h3>';
        return;
    }
    let array = eval(json);
    for (i = 0; i < array.length; i++) {
        let html = '<div><br/><strong>' + array[i].name + '</strong><br/><video id="video_' + array[i].id + '" class="flex-inner video-js vjs-default-skin" controls width="512" height="288"></video></div>';
        document.getElementById('container').innerHTML += html;
    }
    
    let host = rtmp == '' ? hostname : rtmp;
    for (i = 0; i < array.length; i++) {
        var player = videojs('video_' + array[i].id, {
            controls: true,
            sources: [{src: 'rtmp://'+ host + '/' + array[i].channel + '/' + array[i].name, type: 'rtmp/mp4'}],
            techOrder: ['flash', 'html5']
        });
        player.load();
        player.play();
    }
}

function history() {
    fetch('/api/history', {
        headers: {
              'Content-Type': 'application/json;charset=UTF-8',
              'Authorization': localStorage.getItem('token')
        },
        method : 'GET',
        cache: 'no-cache'
    })
    .then(response => response.status == 401 ? unauthorized() : response.json())
    .then(json => appendRec(json))
    .catch(error => console.log(error));
}

function appendRec(json) {
    table = new Tabulator("#content", {
        data:json,
        selectable:true,
        layout:"fitColumns",
        responsiveLayout:"hide",
        tooltips:false,
        addRowPos:"top",
        history:false,
        pagination:"local",
        paginationSize:20,
        movableColumns:false,
        movableRows:false,
        groupBy:"group",
        groupToggleElement:"header",
        groupStartOpen:true,
        initialSort:[{ column:"date", dir:"desc" }],
        placeholder:"No Data Set",
        // rowClick: function(e, row) { // Trigger an alert message when the row is clicked.
        //     if (flvjs.isSupported()) {
        //         appendVideo(row.getData());
        //     }
        // },
        columns:[
            {title:"Device", field:"name", sorter:"string", headerFilter:true},
            {
                title:"Start time", 
                field:"date", 
                sorterParams:{ format:"hh:mm:ss" }, 
                headerFilter:true, 
                formatter:"datetime",
                formatterParams:{outputFormat:"HH:mm:ss"}
            }
            //{title:"Duration in minutes", field:"duration", sorter:"number", headerFilter:true}
        ],
    });

    let btn = document.createElement("BUTTON");
    btn.setAttribute("title", "Add selected streams to playlist");
    btn.setAttribute("class", "tabulator-page");
    btn.setAttribute("style", "float: left");
    btn.onclick = appendVideos;
    btn.appendChild(document.createTextNode("+ Add selected streams"));
    document.getElementsByClassName("tabulator-paginator")[0].prepend(btn);

    let clearTime = document.createElement("BUTTON");
    clearTime.setAttribute("title", "Remove filters");
    clearTime.setAttribute("class", "tabulator-page");
    clearTime.setAttribute("style", "float: left");
    clearTime.onclick = removeFilter;
    clearTime.appendChild(document.createTextNode("X"));
    document.getElementsByClassName("tabulator-paginator")[0].prepend(clearTime);

    let endTime = document.createElement("INPUT");
    endTime.setAttribute("id", "endTime");
    endTime.setAttribute("title", "Select end to the timeline");
    endTime.setAttribute("placeholder", "End time");
    endTime.setAttribute("class", "tabulator-page");
    endTime.setAttribute("style", "float: left");
    document.getElementsByClassName("tabulator-paginator")[0].prepend(endTime);

    let startTime = document.createElement("INPUT");
    startTime.setAttribute("id", "startTime");
    startTime.setAttribute("title", "Select start in the timeline");
    startTime.setAttribute("placeholder", "Start time");
    startTime.setAttribute("class", "tabulator-page");
    startTime.setAttribute("style", "float: left");
    document.getElementsByClassName("tabulator-paginator")[0].prepend(startTime);
    
    if (json.length > 0) {
        let minDate = new Date(Math.min.apply(null, json.map(function(o) { return new Date(o.date); })));
        let maxDate = new Date(Math.max.apply(null, json.map(function(o) { return new Date(o.date); })));
        let e = tail.DateTime(endTime, {
            position: "bottom",
            dateStart: minDate,
            dateEnd: maxDate,
            timeHours: maxDate.getHours(),
            timeMinutes: maxDate.getMinutes(),
            timeSeconds: maxDate.getSeconds()
        });
        let s = tail.DateTime(startTime, {
            position: "bottom",
            dateStart: minDate,
            dateEnd: maxDate,
            timeHours: minDate.getHours(),
            timeMinutes: minDate.getMinutes(),
            timeSeconds: minDate.getSeconds()
        });
        e.on("change", function(){
            periodFilter();
        });
        s.on("change", function(){
            periodFilter();
        });
        //t.selectDate(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate());
    }
}

function customFilter(data, filterParams) {
    let date = new Date(data.date);
    if (filterParams.startDate !== '' && filterParams.endDate === '') {
        return date >= filterParams.startDate;
    }
    else if (filterParams.startDate === '' && filterParams.endDate !== '') {
        return date <= filterParams.endDate;
    }
    else {
        return date <= filterParams.endDate && date >= filterParams.startDate;
    }
}

function periodFilter() {
    let start = document.getElementById('startTime').value;
    let end = document.getElementById('endTime').value;
    let startDate = '';
    let endDate = '';
    if (start !== '') {
        startDate = moment(document.getElementById('startTime').value, 'YYYY-MM-DD HH:mm:ss').toDate();
    }
    if (end !== '') {
        endDate = moment(document.getElementById('endTime').value, 'YYYY-MM-DD HH:mm:ss').toDate();
    }
    table.deselectRow();
    let json = { 'startDate': startDate, 'endDate': endDate };
    table.setFilter(customFilter, json);   
}

function removeFilter() {
    document.getElementById('startTime').value = '';
    document.getElementById('endTime').value = '';
    table.clearFilter();
}

function appendVideo(item) {
    if (videos.indexOf(item) < 0) {
        videos.push(item);
    }
}

function appendVideos() {
    var items = table.getSelectedData();
    if (items.length == 0) {
        alert('Attention, no stream selected!');
        return;
    }
    for (var i = 0; i < items.length; i++) {
        appendVideo(items[i]);
    }
    table.deselectRow();
    refreshVideos();
}

function refreshVideos() {
    let url = proxy == '' ? hostname + ':8080' : proxy;
    document.getElementById('videoContainer').innerHTML = '';
    for (i = 0; i < videos.length; i++) {
        let id = videos[i].id;
        var video = '<div id="item_' + id + '"><strong>' + videos[i].name + '</strong>';
        video += '<button class="tabulator-page" onclick="removeVideo(\'' + id + '\')">X</button><br/>';
        video += convertDateToUTC(videos[i].date).toTimeString() + '<br/>';
        video += '<video id="video_' + id + '" width="512" height="288" controls>';
        video += '<source src="http://' + url + '/' + videos[i].path + '/' + videos[i].filename + '" type="video/mp4">';
        video += '</video></div>';
        document.getElementById('videoContainer').innerHTML += video;
    }
    document.getElementById('playButton').disabled = videos.length == 0;
}

function removeVideo(id) {
    let index = videos.findIndex(p => p.id == id);
    videos.splice(index, 1);
    let divId = 'item_' + id; 
    document.getElementById(divId).remove();
    document.getElementById('playButton').disabled = videos.length == 0;
}

function clearAll() {
    for (i = videos.length - 1; i >= 0; i--) {
        removeVideo(videos[i].id);
    }
    videos = [];
}

function playAll() {
    for (i = 0; i < videos.length; i++) {
        let id = 'video_' + videos[i].id;
        document.getElementById(id).play();

// var player = new MediaElementPlayer(id, {
//     duration: 60 * 5,
//     preload:true,
//     autoplay: true,
//     renderers: ['html5', 'flash_video', 'native_flv', 'youtube_iframe'],
//     plugins: ['flash','silverlight'],
//     features: ['playpause','progress','current','duration','tracks','volume','fullscreen'],
//     forceLive: false,
//     success: function (media, player) {
//         media.play();
//     }
// });
// player.setSrc('//' + hostname + ':8080/' + videos[i].path + '/' + videos[i].filename);

// let videoElement = document.getElementById(id);
// if (!videoElement.hasAttribute("controls")) {
//     videoElement.setAttribute("controls","controls");

//     let flvPlayer = flvjs.createPlayer({
//         type: 'flv',
//         isLive: true,
//         stashInitialSize: '1024KB',
//         duration: videos[i].size,
//         url: 'http://' + hostname + ':8080/' + videos[i].path + '/' + videos[i].filename
//     });
//     flvPlayer.attachMediaElement(videoElement);
//     flvPlayer.load();
//     //setTimeout(function() { flvPlayer.play(); }, 100);
//     flvPlayer.play();
// }
    }
}

// function flvDestroy(player) { 
//     player.pause(); 
//     player.unload(); 
//     player.detachMediaElement(); 
//     player.destroy(); 
//     player = null; 
// } 

function convertDateToUTC(dateString) {
    let date = new Date(dateString);
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()); 
}

function deleteAll() {
    var r = confirm("Are you sure you want to delete all videos?");
    if (r == true) {
      location.href = '/api/history/delete';
    }
}
